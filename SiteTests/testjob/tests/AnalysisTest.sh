#!/bin/sh

# Choose CMSSW version
cmsswvsn=CMSSW_9_2_6

# Check that scramv1 command was defined by . $CMS_PATH/cmsset_default.sh
which scramv1 > /dev/null 2>&1
sset=$?
if [ $sset -ne 0 ]
then 
   echo "ERROR: scramv1 not found"
   echo "summary: SCRAM_NOT_FOUND"
   exit $SAME_ERROR
fi

# Get Working Directory
current=`pwd`

# Set up CMSSW 
tmpfile=`mktemp /tmp/tmp.scram.XXXXXXXXXX`
echo "scramv1 project CMSSW $cmsswvsn ... starting"
scramv1 project CMSSW $cmsswvsn > $tmpfile 2>&1
scms=$?
if [ $scms -ne 0 ]
then
   cat $tmpfile
   echo "ERROR: $cmsswvsn not available"
   echo "summary: CMSSW_NOT_FOUND"
   exit $SAME_ERROR
fi
printf "scramv1 project CMSSW $cmsswvsn ... completed\n"
cd $cmsswvsn/src
export SCRAM_ARCH=`scramv1 arch`
eval `scramv1 runtime -sh`

# Return to working directory 
cd $current

# There are all files available in SAM dataset, which is at all sites
/bin/cat > possible-input-files <<EOI
/store/mc/SAM/GenericTTbar/AODSIM/CMSSW_9_0_0_90X_mcRun1_realistic_v4-v1/10000/28B9D1FB-8B31-E711-AA4E-0025905B85B2.root
/store/mc/SAM/GenericTTbar/AODSIM/CMSSW_9_0_0_90X_mcRun1_realistic_v4-v1/10000/887C13FB-8B31-E711-BCE7-0025905B85BA.root
EOI

nFiles=`cat possible-input-files | wc -l`

# Pick a random integer in [1,nFiles]
d=`date +%N|tr -d 0`
let r=d%nFiles+1

nl=0
until [ $nl -eq $r ]
do
 let nl=nl+1
 read line
done < possible-input-files

inputFile=$line
echo inputFile: $inputFile
pfn=`edmFileUtil -d $inputFile`
if [ -z "$pfn" ] ; then
   echo "WARNING: PFN not returned, using LFN" 
else
   echo PFN: $pfn
   echo $pfn | grep -q '^/'
   res=$?
   if [ $res == 0 ] ; then
      pfn="file:$pfn"
   fi
   inputFile=$pfn
fi
# Create configuration file for cmsRun following the example in
# http://cmslxr.fnal.gov/lxr/source/FWCore/Integration/test/testRunMergeMERGE_cfg.py#006
/bin/cat > analysis_test.py <<EOI
import FWCore.ParameterSet.Config as cms

process = cms.Process("CopyJob")
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 100

process.source = cms.Source("PoolSource",
     fileNames = cms.untracked.vstring("${inputFile}")
)

process.out = cms.OutputModule("PoolOutputModule",
     fileName = cms.untracked.string('myout.root'),
#     fastCloning = cms.untracked.bool(False),
     outputCommands = cms.untracked.vstring('drop *')
)
process.e = cms.EndPath(process.out)
EOI

# Run cmsRun
echo
echo "START TIME: `date` --> running cmsRun -j fjr.xml -p analysis_test.py"
echo
start=`date +%s`
cmsRun -j fjr.xml -p analysis_test.py 2>&1
srun=$?
echo "END TIME: `date`"
stop=`date +%s`
echo "ELAPSED TIME: $[ $stop - $start ] s"
echo

if [ $srun -ne 0 ] ; then
    echo ""
    echo "================= cmsRun configuration file: analysis_test.py ====================="
    echo ""
    cat analysis_test.py
    echo ""
fi

# Check FJR for errors
grep -q "FrameworkError " fjr.xml
sfjr=$?
if [ $sfjr -eq 0 ]
then
   echo "ERROR: FrameworkError found in FJR"
   grep "FrameworkError " fjr.xml
   # if possible extract error
   which xml_grep > /dev/null 2>1 && xml_grep FrameworkError fjr.xml
fi

# Check cmsRun exit status
if [ $srun -ne 0 -o $sfjr -eq 0 ]
then
   echo "ERROR: cmsRun failed with exit code $srun"
   echo "summary: CMSRUN_FAILED_CODE_$srun"
   exit $SAME_ERROR
fi

echo "Test succeeded"
echo "summary: OK"
exit $SAME_OK
