FROM gitlab-registry.cern.ch/etf/docker/etf-exp:latest

LABEL maintainer="Marian Babik <Marian.Babik@cern.ch>"
LABEL description="WLCG ETF CMS"
LABEL version="1.0"

ENV NSTREAM_ENABLED=0

# OSG Middleware
RUN yum -y install yum-priorities
RUN rpm -Uvh https://repo.opensciencegrid.org/osg/3.4/osg-3.4-el7-release-latest.rpm
RUN sed "7i priority=99" -i /etc/yum.repos.d/epel.repo

# Core deps
RUN yum -y install voms globus-gsi-sysconfig globus-gsi-cert-utils globus-gssapi-gsi globus-gss-assist \
                   globus-gsi-proxy-core globus-gsi-credential globus-gsi-callback globus-gsi-openssl-error \
                   globus-openssl-module globus-gsi-proxy-ssl globus-callout

# Condor client
RUN yum -y install condor condor-python

# MW env
COPY docker/etf-cms/config/grid-env.sh /etc/profile.d/
RUN echo "source /etc/profile.d/grid-env.sh" >> /opt/omd/sites/$CHECK_MK_SITE/.profile

# VOMS config
# RUN mkdir -p /etc/vomses/
# COPY ./config/cms-lcg-voms2.cern.ch /etc/vomses/
# COPY ./config/cms-voms2.cern.ch /etc/vomses/
#RUN mkdir -p /etc/grid-security/vomsdir/cms/
#COPY ./config/lcg-voms2.cern.ch.lsc /etc/grid-security/vomsdir/cms/
#COPY ./config/voms2.cern.ch.lsc /etc/grid-security/vomsdir/cms/

# ETF base plugins
RUN yum -y install nagios-plugins-wlcg-condor nagios-plugins-globus nagios-plugins

# ETF JS setup + CMS metrics
RUN yum -y install python-jess python-nap && chmod 755 /usr/lib64/nagios/plugins/check_js
COPY ./docker/etf-cms-dodas/config/check_condor.cfg /etc/ncgx/metrics.d/
COPY ./docker/etf-cms-dodas/config/metrics.cfg /etc/ncgx/metrics.d/wlcg_cms.cfg

# ETF streaming
RUN mkdir -p /var/spool/nstream/outgoing && chmod 777 /var/spool/nstream/outgoing
RUN mkdir /etc/stompclt
COPY docker/etf-cms/config/ocsp_handler.cfg /etc/nstream/

# CMS config
COPY docker/etf-cms/config/cms_checks.cfg /etc/ncgx/conf.d/
COPY docker/etf-cms-dodas/config/etf_plugin_cms_dodas.py /usr/lib/ncgx/x_plugins/

# CMS payload
RUN mkdir -p /usr/libexec/grid-monitoring/probes/org.cms/wnjob
COPY SiteTests/SE/* /usr/libexec/grid-monitoring/probes/org.cms/
COPY nagios/config/org.cms.lcgadmin /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms.lcgadmin
COPY nagios/config/org.cms.production /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms.production
COPY nagios/org.cms.glexec /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms.glexec
COPY SiteTests/MonteCarlo /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/
COPY SiteTests/testjob/tests /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/tests
COPY SiteTests/FroNtier/tests /usr/libexec/grid-monitoring/probes/org.cms/wnjob/org.cms/probes/org.cms/testjob/tests
COPY nagios/config/cms_glexec-etf /etc/cron.d/cms_glexec

# ETF config
#COPY ./config/service_template.tpl /etc/ncgx/templates/
COPY docker/etf-cms-dodas/config/ncgx.cfg /etc/ncgx/

EXPOSE 443 6557
COPY docker/etf-cms/docker-entrypoint.sh /
ENTRYPOINT /docker-entrypoint.sh
