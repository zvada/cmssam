#!/bin/bash --login

# Source UI environment
source `dirname $0`/ui.sh
if [ $? -ne 0 ] ; then
  /bin/echo "--- FATAL: error in sourcing the environment"
  exit 1
fi

# Define location of the cron jobs
SAME_CRON=$HOME/same-cron

if [ -e $SAME_CRON/lock-ce ] ; then
  if [ $((`/bin/date +%s` - `/usr/bin/stat -c %Y $SAME_CRON/lock-ce`)) -ge 21600 ] ; then
    /bin/echo "--- Sensor execution skipped, lock file present and older than 6 hours"
  fi
  exit 0
fi
/bin/touch $SAME_CRON/lock-ce
if [ $? -ne 0 ] ; then
  /bin/echo "--- FATAL: could not create lock file"
  exit 1
fi

# Define location of proxy
TMPFILE=`mktemp`
if [ $? -ne 0 ] ; then
  /bin/rm -f $SAME_CRON/lock-ce
  /bin/echo "--- FATAL: could not create temp file"
  exit 1
fi

#Define the location of SAM
SAME_DIR=$HOME/same

#Define working directory for this SAM instance. The actual directory will be $HOME/.$SAME_WORKDIR
SAME_WORKDIR=`basename $SAME_DIR`

# Clean up old work files
/usr/bin/find $HOME/.same/*/nodes/* -maxdepth 0 -mtime +2 -ls -exec /bin/rm -rf {} \;

# Define which sensors to run and to publish
publish_sensors="CE CREAMCE"
sensors="CE CREAMCE SRMv2"

echo -n "STARTTIME: "
date "+%Y-%m-%d %H:%M:%S"

# Update CMS scripts from CVS (comment if you do not want automatic updates)
$SAME_CRON/cms2sam.sh $SAME_DIR 2>&1
if [ $? -ne 0 ] ; then
  /bin/echo "--- FATAL: could not update CMS files"
  exit 1
fi

# Update list of CMS sites to test (comment if you do not want automatic updates)
$SAME_CRON/makesitelist.pl $SAME_DIR/client/etc/same.conf.CMS $SAME_DIR/client/etc/same.conf $SAME_WORKDIR 2>&1

# Use WMS rather than RB
/bin/cp -f $SAME_DIR/client/sensors/CE/config-wms.sh $SAME_DIR/client/sensors/CE/config.sh

$SAME_CRON/makesrmlist.pl $SAME_DIR/client/etc/same.conf.CMS $SAME_DIR/client/etc/same_SRMv2.conf 2>&1

/bin/cp -f $SAME_DIR/client/sensors/SRMv2/test-sequence-user.lst $SAME_DIR/client/sensors/SRMv2/test-sequence.lst

# Blacklist specific CE instances
export SAME_HOME=$SAME_DIR/client

/bin/echo ""
/bin/echo ""
/bin/echo -n "----------------------[ "
/bin/date

# Define the location of your certificate
export X509_USER_CERT=/afs/cern.ch/user/s/samcms/.globus-asciaba/usercert.pem
export X509_USER_KEY=/afs/cern.ch/user/s/samcms/.globus-asciaba/userkey.pem
export X509_USER_PROXY=$TMPFILE

# Choose an FQAN and a lifetime for the VOMS extensions
fqan='/cms/Role=lcgadmin'
lifetime='12:00'

/bin/echo "--- Using the $fqan FQAN"

voms-proxy-destroy
voms-proxy-init -debug -valid $lifetime -voms cms:$fqan -pwstdin < /afs/cern.ch/user/s/samcms/.pawlogin 2>&1
if [ $? != 0 ] ; then
  /bin/echo "--- FATAL: proxy creation failed!"
  /bin/rm -f $SAME_CRON/lock-ce
  exit 1
fi  

for sensor in $publish_sensors ; do
  /bin/echo ""	
  /bin/echo "----------"
  /bin/echo "Publishing $sensor sensor: "
  /bin/echo ""
  $SAME_DIR/client/bin/same-exec --publish $sensor 2>&1
done

for sensor in $sensors ; do
  CONF=$SAME_DIR/client/etc/same.conf
  if [ $sensor == 'SRMv2' ] ; then
    CONF=$SAME_DIR/client/etc/same_SRMv2.conf
  fi
  /bin/echo ""
  /bin/echo "----------"
  /bin/echo "Executing $sensor sensor: "
  /bin/echo ""
  $SAME_DIR/client/bin/same-exec -c $CONF $sensor 2>&1
done

/bin/echo -n "ENDTIME: "
/bin/date "+%Y-%m-%d %H:%M:%S"

/bin/rm -f $TMPFILE
/bin/rm -f $SAME_CRON/lock-ce
